%EXP3 algorithm is an extension of the Hedge algorithm for the adversarial MAB problem with partial information, i.e. only the reward of the selected action can be observed.

classdef EXP3 <handle
    
    properties
        n_arms; %number of actions
        gamma; %egalitarianism factor [0,1), defines amount of exploration, the closer to 1 the more exploration the agent does
        w; %weights to decide randomly which action to take next
        p; %distribution
        
        %For the regret calculation
        cumulativeReward;
    end
    
    methods
        
        function self = EXP3(varargin)
            self.n_arms = varargin{1};
            self.gamma = varargin{2};
            self.w = ones(self.n_arms,1);
            self.p = zeros(self.n_arms,1);
            
            %For the regret calculation
            self.cumulativeReward = 0;
        end
        
        function set_probability(self)
            for i=1:self.n_arms
                %Each value p(i) is a probability for an action i
                %gamma, the closer to 1 (then the weights are less meaningful), the more exploration the agent does
                self.p(i) = (1-self.gamma)* (self.w(i)/sum(self.w)) + (self.gamma/self.n_arms); 
            end
        end
        
        %Randomization is introduced into the action selection process (mixed strategy),
        %That is, instead of selecting a single action at each t, the player chooses a probability distribution Pt = (p_1,t,..., p_K,t) over the set of K actions, and plays action i with probability p_i,t .
        %Returns index of arm based on probabilities
        function idx =  drawactionaccording_probability(self)
            normalised_weights = zeros(self.n_arms,1);
            %Normalize the weights
            for i=1:self.n_arms
                normalised_weights(i) = self.p(i)/sum(self.p);
            end

            %Random generator for arm index based on probabilities
            randnb = rand;
            idx = 1;
            r_s = 0;
            r_e = 0;
            for i=1 : self.n_arms
                r_e = r_e + normalised_weights(i,1);
                if randnb >r_s && randnb <= r_e
                    idx = i;
                    break;
                end
                r_s = r_e;
            end
        end
        
        %In each step/trial/round, the player has access to the history of rewards for 
        %the actions that were chosen by EXP3 in previous rounds, but not the rewards of unchosen actions.
        %Computing weights for next trial
        function update_weightofselectedarm(self,arm,reward)
            %Define the estimated reward for each arm/action to compensate the fact that the probability of chosen action i is small:
            estimatedReward = reward/self.p(arm);
            %We only change the weight of the selected arm:
            self.w(arm)=self.w(arm)*exp(self.gamma*estimatedReward/self.n_arms);
        end
    end
end
