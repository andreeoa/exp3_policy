## EXP3

Bandit policy to trade-off between exploration and exploitation.

## Parameters

* n_arms - Actions
* gamma - Exploration rate

## Acknowledgments

* Auer, P., Cesa-Bianchi, N., Freund, Y., & Schapire, R. E. (2002). The nonstochastic multiarmed bandit problem. SIAM journal on computing, 32(1), 48-77.
